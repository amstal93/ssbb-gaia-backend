import Sequelize from 'sequelize';
import config from '../../config/vars';

const fs = require('fs');
const path = require('path');

const {
  database,
  username,
  password,
  host,
  dialect,
  port,
} = config.sequelize;

const sequelize = new Sequelize(
  database,
  username,
  password, {
    host,
    dialect,
    port,
  },
);

const db = {};
fs
  .readdirSync(__dirname)
  .filter(file => (!file.includes('.js.map')) && (file !== 'index.model.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });
Object.keys(db).forEach((modelName) => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db);
    console.log('Model', modelName);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;

/* const Project = Sequelize.define('Project', {
  title: Sequelize.STRING,
  description: Sequelize.TEXT
});
Project.sync(); */
